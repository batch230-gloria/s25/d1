// console.log("monday")

// console.log("================= JSON Object =================")
// [SECTION] JSON Object
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages
	- Core JavaScript has a built in JSON objects that contains methos for passing JSON objects
	- JSON is used for serializing different data types into bytes
*/

// JSON Object
/*
	- JSON also uses "key/value pairs" just like the object properties in JavaScript
	- Key/Property names should be enclosed with double quotes

	Syntax:
	{
		"propertyA" : "valueA",
		"propertyB" : "valueB"
	}

*/

/*
{
	"city" : "Quezon City",
	"province" : "Matro Manila",
	"country" : "Philippines"
}
*/

// console.log(" ---------------- JSON Arrays ---------------- ");
// JSON Arrays
// Array in JSON are almost same as arrays in JavaScript
// Array in JSON Object

/*
	"cities" : [
	{
		"city" : "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
	{
		"city" : "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"		
	},
	{
		"city" : "Makati City",
		"province" : "Metro Manila",
		"country" : "Philippines"		
	}
	]
*/



console.log("================= JSON Methods =================")
// [SECTION] JSON Methods
// The "JSON Object" contains methods for parsing and converting data in stringified JSON
// JSON data is sent or received in text-only(String) format

// Converting Data Intro Stringified JSON
console.log(" ---------------- JSON stringify ---------------- ");
let batchesArr = [
		{
			batchName: 230,
			schedule: "Part Time"
		},
		{
			batchName: 240,
			schedule: "Full Time"
		}

	];

console.log(batchesArr);

console.log("Result from stringify method: ");
/*
	Syntax: 
		JSON.stringify(arrayName/objectName);
*/ 

console.log(JSON.stringify(batchesArr));

// Example 2
let data = JSON.stringify(
		{
			name: "John",
			age: 31,
			address: {
				city: "Manila",
				country: "Philippes"
			}
		}

	);

console.log(data);

/*console.log ("------ Mini Quiz -------")''
// User Details
let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email");
let password = prompt("Enter your password");

let user = JSON.stringify(
		{
			firstName: firstName,
			lastName: lastName,
			email: email,
			password: password
		}
	)

console.log(user);
*/


// Converting Stringigied JASON into JavaScript Objects:

let batchesJSON=`[
	{
		"batchName": 230,
		"schedule": "Part Time"
	},
	{
		"batchName": 240,
		"schedule": "Full Time"
	}
]
`;

console.log("batchesJSON content: ");
console.log(batchesJSON);

console.log("Result from parse method: ");
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);


let stringifiedObject = `
	{
		"name" : "John",
		"age" : 31,
		"address" :{
			"city" : "Manila",
			"country" : "Philippes"
		}
	}
`
console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));
